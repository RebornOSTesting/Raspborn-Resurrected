#!/bin/bash

echo "Please be sure to run this script as sudo."
echo "Please select the location of your SD Card. ALL DATA WILL BE DESTROYED."

echo "Device name (usually /dev/mmcblkX or /dev/sdX):"

read drive

echo "You have selected $drive, if this is not correct, please use CTRL+c to terminate this script."
echo "Please wait 10 seconds for the script to begin..."

sleep 10s

echo "Beginning the process of formatting $drive.  Please standby..."

# We will be creating partitions programmatically versus relying on user input.
# The script below will create two partitions (automatically resizing the last) for the RPi. Blank lines indicate the default selection.  This will wipe all data on the selected disk.
sudo fdisk ${drive} 2>&1 <<EOF
o
n
p
1

+100M
t
c
n
p
2


w 
EOF

echo "Formatting $drive completed.  Please standby while the filesystems are being created..."

# The section below handles creating the file systems on the newly created partitions, and mounting them.

echo "Creating filesystem for partition 1, please type yes if prompted"
# Unmount newly partitioned drives (some distros auto-mount new partitions)
sudo umount ${disk}1
sudo umount ${disk}2
# Make a vfat filesystem on the first partition.
sudo mkfs.vfat ${drive}1
# Make a directory in the current location for the RPi boot image.
mkdir boot
# Mount the newly created partition on the newly created directory.
sudo mount ${drive}1 boot

echo "Filesystem created for partition 1, moving to partition 2"

# Repeat for the second partition.

echo "Creating filesystem for partition 2, please type yes if prompted"
# Make an ext4 filesystem.
sudo mkfs.ext4 ${drive}2
# Make a folder to mount partition 2.
mkdir root
# Mount partition 2
sudo mount ${drive}2 root

echo "Filesystem created for partition 2.  All filesystems created."

echo "Filesystems mounted.  Downloading ArchLinux for Raspberry Pi 2, please standby..."

echo "Using wget to pull the latest ArchLinux ARM image from os.archlinuxarm.org..."
# Download current ArchLinux ARM image to current working directory
wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-3-latest.tar.gz
echo "Download completed.  Expanding ArchLinuxARM image.  This may take some time."
# Extract the image and copy to partition 2 mounted on root/
sudo bsdtar -xpf ArchLinuxARM-rpi-3-latest.tar.gz -C root/
echo "Image expansion complete.  Moving image to $drive.  This may take some time depending on drive size"
# Synchronize the changes to the filesystem.
sync
# Move the boot stuff from Partition 2 to Partition 1.
sudo mv root/boot/* boot/
echo "File transfer completed...Please wait until the drive is successfully unmounted..."
# Unmount both partitions.
sudo umount boot root
echo "$drive unmounted.  You may now place the SD card in your RPi, connect ethernet and power, and boot into your ArchLinux ARM system!"

echo "Doing some last minute cleaning...please standby"
# Just leaving things how we found them...Remove the image...
sudo rm ArchLinuxARM-rpi-3-latest.tar.gz
# Remove the mount points....
sudo rm -R root/ boot/
echo "Cleaning complete!"
# And done!
echo "Default user login is alarm/alarm"
echo "Default root login is root/root"

