Raspborn is in a pre-alpha state.  With that said there are no explicit gaurantees that Raspborn won't do any or all of the following:

1) Boot
2) Install
3) Not brick your device
4) Steal your car
5) Not corrupt your SD Card
6) Any other negative thing you can imagine...

With that out of the way, copy the files to a MicroSD card as you would for a traditional NOOBS installation.  When you insert the
SD Card into your RPi you should be greeted with a text-based installer for ArchLinux.  Work through the prompts per usual.  Report any issues.

Minimum requirements:

1) Raspberry Pi 2/3/3B/4 (Choose appropriate script)
2) MicroSD card with a minimum of 8GB (Script will remove all existing partitions on target device and create new partitions)
3) Stable wired internet connection (on the todo list), currently necessary for a headache free first-boot
   unless you are OK with muddling around the command line for awhile ('sudo nmtui' or 'sudo wifi-menu' may work though aren't tested)


Known Issues:

1) Wifi cannot be used to install the OS
2) Currently working on adding the individual scripts to the installation menu.