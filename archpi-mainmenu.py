# Import the necessary packages
from consolemenu import *
from consolemenu.items import *

# Create the menu
menu = ConsoleMenu("RebornOS - Raspberry Pi Edition", "Made for you, made with you")

# Create some items

# MenuItem is the base class for all items, it doesn't do anything when selected
# menu_item = MenuItem("Menu Item")

# A FunctionItem runs a Python function when selected
# function_item = FunctionItem("Call a Python function", input, ["Enter an input"])

# A CommandItem runs a console command
view_mounts = CommandItem("View Mounted Disks",  "/usr/bin/xterm -hold -e 'sudo sfdisk -l | grep 'Disk /dev' | awk '{print $2}' | rev | cut -c 2- | rev > test.txt && cat test.txt && rm test.txt'")
vanilla_alarm_rpi4= CommandItem("Vanilla ArchLinuxARM - Raspberry Pi 4", "/usr/bin/xterm -hold -e 'sudo wget -O - https://gitlab.com/RebornOSTesting/Raspborn-Resurrected/-/blob/master/archpiscript.sh | bash'")
vanilla_alarm_rpi3 = CommandItem("Vanilla ArchLinuxARM - Raspberry Pi 3/B+", "/usr/bin/xterm -hold -e 'sudo wget -O - https://gitlab.com/RebornOSTesting/Raspborn-Resurrected/-/blob/master/archpiscript-rpi3.sh | bash'")
vanilla_alarm_rpi2 = CommandItem("Vanilla ArchLinuxARM - Raspberry Pi 2", "/usr/bin/xterm -hold -e 'sudo wget -O - https://gitlab.com/RebornOSTesting/Raspborn-Resurrected/-/blob/master/archpiscript-rpi2.sh | bash'")
rum_pi = CommandItem("Include RUMPi (Reborn Update Manager Pi Edition) install script", "/usr/bin/xterm -hold -e 'sudo wget -O - https://gitlab.com/RebornOSTesting/Raspborn-Resurrected/-/blob/master/archpi-RUMPi.sh | bash'")

# A SelectionMenu constructs a menu from a list of strings
# selection_menu = SelectionMenu([command_item, "Install OS to SD Card", "Expand FS"])

# A SubmenuItem lets you add a menu (the selection_menu above, for example)
# as a submenu of another menu
# submenu_item = SubmenuItem("Install for Raspberry Pi 4", selection_menu, menu)

# Once we're done creating them, we just add the items to the menu
# menu.append_item(menu_item)
# menu.append_item(function_item)
menu.append_item(view_mounts)
menu.append_item(vanilla_alarm_rpi4)
menu.append_item(vanilla_alarm_rpi3)
menu.append_item(vanilla_alarm_rpi2)
menu.append_item(rum_pi)
# menu.append_item(submenu_item)

# Finally, we call show to show the menu and allow the user to interact
menu.show()